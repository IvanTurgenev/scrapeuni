package main

import (
	// "encoding/csv"
	"fmt"
	// "os"

	"github.com/gocolly/colly/v2"

	// "io/ioutil"
	"log"

	// "github.com/davecgh/go-spew/spew"

	"os"
	// "encoding/csv"
	// "io"
	"bytes"
	"encoding/gob"
	"errors"
	"os/signal"
	"strconv"
	"strings"

	db "gitlab.com/IvanTurgenev/scrapeuni/database"
	"gopkg.in/ini.v1"

	// "time"
	// "net"
	// "net/url"
	"syscall"
	"unicode"
	// "github.com/PuerkitoBio/goquery"
	// "net/http"
	// "github.com/PuerkitoBio/goquery"
)

// var websiteURL = "https://www.mastersportal.com/studies/"

// const dbname string = "./dbstore/badger-master.db"
// const dbfolder = "./dbstore"
// const typeStudy = "master"
// // const studies = "phdportal.com/studies/"

// var allowedDomains = []string{"mastersportal.com", "www.mastersportal.com"}

var websiteURL string

var dbname string
var dbfolder string
var typeStudy string

var allowedDomains = []string{}

// var websiteURL = "https://www.mastersportal.com/studies/"

// const dbname string = "./dbstore/badger-master.db"
// const dbfolder = "./dbstore"
// const typeStudy = "master"

// var allowedDomains = []string{"mastersportal.com", "www.mastersportal.com"}

var database db.Database

type lastgb struct {
	Bad  int
	Good int
}

type coniig struct {
	WebsiteURL     string
	DBname         string
	DBFolder       string
	TypeStudy      string
	AllowedDomains string
}

var good int
var bad int

type uProgram struct {
	ProgramName         string
	DegreeType          string
	ApplicationDeadline string
	Duration            string
	Tuition             string
	SchoolName          string
	Location            string
	Website             string
	Overview            string
	Programme           string
	KeyFacts            string
	Admission           string
	TuitionFee          string
	LivingCost          string
	Funding             string
	ID                  string
	BodyHTML            string
	URL                 string
	TypeStudy           string
	ImageURL            string
	Tags                []string
}

func globalConfig(s string) {
	if s == "bachelor" {
		websiteURL = "https://www.bachelorsportal.com/studies/"

		dbname = "./dbstore/badger-bachelor.db"
		dbfolder = "./dbstore"
		typeStudy = "bachelor"

		allowedDomains = []string{"bachelorsportal.com", "www.bachelorsportal.com"}

	} else if s == "phd" {
		websiteURL = "https://www.phdportal.com/studies/"

		dbname = "./dbstore/badger-phd.db"
		dbfolder = "./dbstore"
		typeStudy = "phd"
		// const studies = "phdportal.com/studies/"

		allowedDomains = []string{"phdportal.com", "www.phdportal.com"}

	} else if s == "master" {
		websiteURL = "https://www.mastersportal.com/studies/"

		dbname = "./dbstore/badger-master.db"
		dbfolder = "./dbstore"
		typeStudy = "master"
		// const studies = "phdportal.com/studies/"

		allowedDomains = []string{"mastersportal.com", "www.mastersportal.com"}

	} else {
		fmt.Println("Exitings bad arguments..")
		os.Exit(1)
	}

}
func encodedb(p uProgram) []byte {
	var b bytes.Buffer
	e := gob.NewEncoder(&b)
	if err := e.Encode(p); err != nil {
		panic(err)
	}
	return b.Bytes()

}

func encodeString(s string) []byte {
	var b bytes.Buffer
	e := gob.NewEncoder(&b)
	if err := e.Encode(s); err != nil {
		panic(err)
	}
	return b.Bytes()

}

func decodeString(b []byte) string {
	var s string
	d := gob.NewDecoder(bytes.NewReader(b))
	if err := d.Decode(&s); err != nil {
		panic(err)
	}
	// fmt.Println(value)
	return s
}

func removeBadSpace(s string) string {
	rr := make([]rune, 0, len(s))
	for _, r := range s {
		if !unicode.IsSpace(r) {
			rr = append(rr, r)
		} else {
			rr = append(rr, ' ')
		}
	}
	return string(rr)
}

func removeDuplicateValues(intSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}

	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func decodeID(s string) string {
	tempss := strings.Split(s, "/")
	return tempss[4]
}
func getdb(key string, db db.Database) (uProgram, error) {
	value, err := db.Get([]byte(key))
	if err != nil {
		return uProgram{}, errors.New("ID doesnt exist in database")
	}

	var tempupProgram uProgram
	d := gob.NewDecoder(bytes.NewReader(value))
	if err := d.Decode(&tempupProgram); err != nil {
		panic(err)
	}
	// fmt.Println(value)
	return tempupProgram, nil
}

func visit(gennum chan int, writeChan chan uProgram, printChan chan lastgb) { // var isgood bool
	// var last lastgb
	// // var to = start
	// var num = start
	var isgood bool
	var tempuProgram uProgram
	var last lastgb
	fmt.Println("Starting...")
	collector := colly.NewCollector(
		colly.AllowedDomains(allowedDomains...),
		// colly.CacheDir(".cache"),
		// colly.AllowURLRevisit(),
	)

	collector.OnHTML(`body`, func(e *colly.HTMLElement) {
		// log.Println("Course found", e.Request.URL)

		programURL := e.Request.URL.String()
		id := decodeID(programURL)
		// name := e.ChildText(`h1 > a[class="StudyLink TrackingExternalLink"]`)
		// s := e.ChildAttr(`li > a`, "href")
		var uimageURL string
		if e.ChildAttr(`a[class="StudyLink TrackingExternalLink Logo"] > img`, "src") != "" {
			uimageURL = "https:" + e.ChildAttr(`a[class="StudyLink TrackingExternalLink Logo"] > img`, "src")
		}
		// uimageURL, err := url.Parse(imageURL)
		// spew.Dump(uimageURL)
		// if err != nil {
		// 	panic(err)
		// }
		bodyHTML, err := e.DOM.Html()
		if err != nil {
			fmt.Println(err)
		}
		if typeStudy == "master" {

			var tags []string
			name := e.ChildText(`header > h1`)
			e.ForEach(`div[class="DegreeTags"]`, func(_ int, el *colly.HTMLElement) {
				tags = append(tags, el.ChildText("span"))
			})
			var location string
			e.ForEach(`span[class="LocationItems"]`, func(_ int, el *colly.HTMLElement) {
				location = location + el.Text
			})
			tempuProgram = uProgram{
				ProgramName: name,
				// DegreeType:          name[strings.LastIndex(name, " ")+1:],
				ApplicationDeadline: e.ChildText(`li[class=" js-openTab OpenTab js-deadlineFact QFDeadline QuickFact HideQFMobile "] > span[class="QFDetails"] > div:nth-child(1)`),
				Duration:            e.ChildText(`div[class="Title"] > span[class="js-duration"]`),
				Tuition:             e.ChildAttr(`span[data-currency-rewrite=""]`, "data-original_html"),
				SchoolName:          e.ChildText(`span[class="NameLocation"] > a[class="Name"]`),
				Location:            location,
				Website:             e.ChildAttr(`article > a[class="StudyLink TrackingExternalLink ProgrammeWebsiteLink"]`, "data-description"),
				Overview:            e.ChildText(`section[id="StudyDescription"]`),
				Programme:           e.ChildText(`article[id="StudyContents"]`),
				KeyFacts:            e.ChildText(`section[id="StudyKeyFacts"]`),
				Admission:           e.ChildText(`section[id="LanguageRequirementsSegmentedControl"]`),
				TuitionFee:          e.ChildText(`section[id="TuitionFeeContainer"]`),
				LivingCost:          e.ChildText(`section[id="LivingCostContainer"]`),
				Funding:             e.ChildText(`section[id="FundingContainer"]`),
				BodyHTML:            bodyHTML,
				URL:                 programURL,
				ID:                  id,
				TypeStudy:           typeStudy,
				ImageURL:            uimageURL,
				Tags:                tags,
			}

		} else {
			name := e.ChildText(`ul[class="LinkTrail"] > li:nth-child(5)`)
			tempuProgram = uProgram{
				ProgramName:         name,
				DegreeType:          name[strings.LastIndex(name, " ")+1:],
				ApplicationDeadline: e.ChildText(`li[class=" js-openTab OpenTab js-deadlineFact QFDeadline QuickFact HideQFMobile "] > span[class="QFDetails"] > div:nth-child(1)`),
				Duration:            e.ChildText(`div[class="js-duration"]`),
				Tuition:             e.ChildAttr(`span[data-currency-rewrite=""]`, "data-original_html"),
				SchoolName:          e.ChildText(`a[class="OrganisationTitle"] > span`),
				Location:            e.ChildText(`span[class="OrganisationCity"]`),
				Website:             e.ChildAttr(`a[class="OrganisationTitle"]`, "href"),
				Overview:            e.ChildText(`section[id="StudyDescription"]`),
				Programme:           e.ChildText(`article[id="StudyContents"]`),
				KeyFacts:            e.ChildText(`section[id="StudyKeyFacts"]`),
				Admission:           e.ChildText(`section[id="LanguageRequirementsSegmentedControl"]`),
				TuitionFee:          e.ChildText(`section[id="TuitionFeeContainer"]`),
				LivingCost:          e.ChildText(`section[id="LivingCostContainer"]`),
				Funding:             e.ChildText(`section[id="FundingContainer"]`),
				BodyHTML:            bodyHTML,
				URL:                 programURL,
				ID:                  id,
				TypeStudy:           typeStudy,
				ImageURL:            uimageURL,
			}
		}
		// spew.Dump(tempuProgram)
		// os.Exit(1)
		isgood = true
	})

	for {
		num := <-gennum
		prourl := websiteURL + strconv.Itoa(num)
		// fmt.Println(prourl)
		collector.Visit(prourl)
		if isgood {
			isgood = false
			last.Good = num
			writeChan <- tempuProgram
		} else {
			last.Bad = num

		}
		printChan <- last
	}

}

func createConfiguration() {
	if fileExists("config.ini") {

	} else {
		fmt.Println("Creating config file")
		cfg := ini.Empty()
		cfg.Section("").NewKey("channelName", "https://www.phdportal.com/studies/")
		cfg.Section("").NewKey("channelID", "xxx")
		cfg.Section("").NewKey("discordWebhookURL", "xxx")
		err1 := cfg.SaveTo("config.ini")
		if err1 != nil {
			fmt.Println(err1)
		}
		fmt.Println("Exiting edit new config file, and start again")
		os.Exit(1)
	}

}

// func opendb() db.Database {
// 	// err := os.MkdirAll(dbfolder, os.ModePerm)
// 	// err := os.Mkdir(dbfolder, 0755)
// 	// if err != nil {
// 	// 	fmt.Println("Error creating dbstore folder")
// 	// 	os.Exit(1)
// 	// }
// 	d, err := db.Open(dbname)
// 	if err != nil {
// 		fmt.Println("I didn't create a new DB")
// 		os.Exit(1)
// 	}
// 	return d
// }
func getFileWriter(writeChan chan uProgram) {
	for {
		tempuProgram := <-writeChan
		setdb(tempuProgram.ID, tempuProgram, database)
		// database.Set([]byte(id),[]byte(imageURL))
		// fmt.Println(url)
		// getTempuProgram, err := getdb(tempuProgram.ID, database)
		// if err != nil {
		// 	os.Exit(1)
		// }
		// if getTempuProgram == tempuProgram {
		// 	fmt.Println("All good")
		// } else {
		// 	fmt.Println("Bad...")
		// }
	}
}

func opendb() {
	err := os.MkdirAll(dbfolder, os.ModePerm)
	d, err := db.Open(dbname)
	if err != nil {
		fmt.Println("I didn't create a new DB")
		os.Exit(1)
	}
	d.Close()
	d, err = db.Open(dbname)
	if err != nil {
		fmt.Println("I didn't create a new DB")
		os.Exit(1)
	}

	database = d
}

func setdb(key string, value uProgram, db db.Database) {
	if err := db.Set([]byte(key), encodedb(value)); err != nil {
		fmt.Println("I didn't set any data")
	}
}

func numberGenerator(start int, genNum chan int) {
	var i int = start
	for {
		genNum <- i
		// i = i + 2
		i++
	}

}

func chanPrint(printChan chan lastgb) {
	var i int = 0
	for {
		last := <-printChan
		if last.Good > good {
			good = last.Good
			i++
		}
		if last.Bad > bad {
			bad = last.Bad
		}
		var goodS = strconv.Itoa(good)
		var badS = strconv.Itoa(bad)
		fmt.Printf("\033[2K\r%s", "Last good ID: "+goodS+" | Last bad ID: "+badS+" | Counter: "+strconv.Itoa(i))
	}
}

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		fmt.Println("Bad arguments...")
		os.Exit(1)

	}
	counterID, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("Bad argument...")
		os.Exit(1)
	}
	globalConfig(args[1])
	opendb()
	defer database.Close()
	// createStore()
	// getFileWriter()
	// progresPrinter()
	// printChann := make(chan lastgb)
	// var numworkers int = 1
	// fmt.Println("Workers starting: " + strconv.Itoa(numworkers))
	writeChan := make(chan uProgram)
	printChan := make(chan lastgb)
	genNum := make(chan int)
	var numworkers int = 1
	go numberGenerator(counterID, genNum)
	go getFileWriter(writeChan)

	fmt.Println("Workers starting: " + strconv.Itoa(numworkers))
	for i := 0; i < numworkers; i++ {
		go visit(genNum, writeChan, printChan)
	} // go visit(2, numworkers, printChann)
	go chanPrint(printChan)
	// go visit(23792, numworkers, printChann)
	// go chanPrint(printChann)
	// log.Println("Listening signals...")
	done := make(chan struct{})

	go func() {
		c := make(chan os.Signal, 1) // we need to reserve to buffer size 1, so the notifier are not blocked
		signal.Notify(c, os.Interrupt, syscall.SIGTERM)

		<-c
		close(done)
	}()

	<-done
	log.Println("Exiting...")
}
