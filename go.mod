module gitlab.com/IvanTurgenev/scrapeuni

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/dgraph-io/badger v1.6.1
	github.com/dgraph-io/badger/v2 v2.0.3
	github.com/gocolly/colly/v2 v2.1.0
	gopkg.in/ini.v1 v1.57.0
)
